/*
    Get a list of items
    Get a single item
    Create a new item
    Update an existing item
    Delete a single item
    Delete an entire list of items
*/

import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { Item } from './item';

@Injectable()
export class ItemService {
  itemsCollection: AngularFirestoreCollection<Item>;
  // todoCollectionRef: AngularFirestoreCollection<Item>;
  items: Observable<Item[]>;
  itemDoc: AngularFirestoreDocument<Item>;

  constructor(public afs: AngularFirestore) {
    // this.todoCollectionRef = this.afs.collection<Item>('listaPozycji'); // a ref to the todos collection
    // this.todo$ = this.todoCollectionRef.valueChanges();
    // console.log(this.todo$);
    this.itemsCollection = this.afs.collection('items', ref =>
      ref.orderBy('title', 'asc')
    );

    this.items = this.itemsCollection.snapshotChanges().map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as Item;
        data.id = a.payload.doc.id;
        return data;
      });
    });
  }

  getItems() {
    return this.items;
  }

  addItem(item: Item) {
    this.itemsCollection.add(item);
  }

  deleteItem(item: Item) {
    this.itemDoc = this.afs.doc(`items/${item.id}`);
    this.itemDoc.delete();
  }

  updateItem(item: Item) {
    this.itemDoc = this.afs.doc(`items/${item.id}`);
    this.itemDoc.update(item);
  }
}
