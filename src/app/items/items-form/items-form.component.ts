import { Component, OnInit } from '@angular/core';
import { Item } from '../shared/item';
import { ItemService } from '../shared/item.service';

@Component({
  selector: 'app-items-form',
  templateUrl: './items-form.component.html',
  styleUrls: ['./items-form.component.scss']
})
export class ItemsFormComponent implements OnInit {
  item: Item = {
    title: '',
    description: ''
  };
  constructor(private itemService: ItemService) {}

  ngOnInit() {}

  onSubmit() {
    if (this.item.title !== '' && this.item.description !== '') {
      this.itemService.addItem(this.item);
      this.item.title = '';
      this.item.description = '';
    }
  }
}
